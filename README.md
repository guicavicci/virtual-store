 
# Virtual Store
4 BRPrev
  
## Definition

  
  

The project uses docker compose to launch a java 11 application, using the following dependency stack:
- Postgresql
- Kafka
- Zookeeper  


## Quick Start

  
  

**Start project**

To start the project you will need to install the following dependencies:

  

[https://docs.docker.com/v17.09/engine/installation/](https://docs.docker.com/v17.09/engine/installation/)

  

[https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

  

It is necessary to clone the following project:

https://gitlab.com/guicavicci/virtual-store/
  
## Virtual Store
Build and run application:

run the command **docker-compose up**

Stop application: **Ctrl + c**


**Endpoint**

http://localhost:8085

#   Sending requests to the application

Import Postman collection:

- Open Postman Application
- Import
- Select the file: virtual-store.postman_collection.json

To authenticate:

- authenticate - returns a token JWT and stores it in the token variable
- client - create
- product - create
- order - create passing a list of products and a customer responsible for the order