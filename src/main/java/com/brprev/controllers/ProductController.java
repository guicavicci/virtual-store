package com.brprev.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.brprev.models.Product;
import com.brprev.payload.ProductDTO;
import com.brprev.services.ProductServiceInterface;

import javassist.NotFoundException;

@RestController
@RequestMapping(value = "/product")
public class ProductController {
	
	@Autowired
	private ProductServiceInterface prodService;
	
	Product prod = new Product();
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Product> save(@RequestBody ProductDTO prodDTO) {
		prod = prodService.save(prodDTO);
		return new ResponseEntity<>(prod, HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Product> update(@RequestBody ProductDTO prodDTO) {
		prod = prodService.save(prodDTO);
		return new ResponseEntity<>(prod, HttpStatus.OK);

		
	}
	
	@RequestMapping(value="/{id}", method= RequestMethod.GET)
	public ResponseEntity<Product> findById(@PathVariable("id") long id) {
		try {
			prod = prodService.findById(id);
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(prod, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method= RequestMethod.DELETE)
	public ResponseEntity<Product> delete(@PathVariable("id") long id) {
		prodService.delete(id);
		return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
	}

}
