package com.brprev.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.brprev.models.Order;
import com.brprev.payload.OrderDTO;
import com.brprev.services.OrderServiceInterface;

import javassist.NotFoundException;

@RestController
@RequestMapping(value = "/order")
public class OrderController {
	
	@Autowired
	private OrderServiceInterface orderService;
	
	Order order = new Order();
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Order> save(@RequestBody OrderDTO orderDTO) {
		try {
			order = orderService.save(orderDTO);
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(order, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method= RequestMethod.GET)
	public ResponseEntity<Order> findById(@PathVariable("id") long id) {
		try {
			order = orderService.findById(id);
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(order, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method= RequestMethod.DELETE)
	public ResponseEntity<Order> delete(@PathVariable("id") long id) {
		orderService.delete(id);
		return new ResponseEntity<Order>(HttpStatus.NO_CONTENT);
	}

}
