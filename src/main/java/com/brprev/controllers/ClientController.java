package com.brprev.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.brprev.models.Client;
import com.brprev.payload.ClientDTO;
import com.brprev.services.ClientServiceInterface;

import javassist.NotFoundException;

@RestController
@RequestMapping(value = "/client")
public class ClientController {
	
	@Autowired
	private ClientServiceInterface cliService;
	
	Client cli = new Client();
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Client> save(@RequestBody ClientDTO cliDTO) {
		 cli = cliService.save(cliDTO, true);
		return new ResponseEntity<>(cli, HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<Client> update(@RequestBody ClientDTO cliDTO) {
		cli = cliService.save(cliDTO, false);
		return new ResponseEntity<>(cli, HttpStatus.OK);

		
	}
	
	@RequestMapping(method= RequestMethod.GET)
	public ResponseEntity<Client> findById(@PathVariable("id") long id) {
		try {
			cli = cliService.findById(id);
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(cli, HttpStatus.OK);
	}
	
	@RequestMapping(method= RequestMethod.DELETE)
	public ResponseEntity<Client> delete(@PathVariable("id") long id) {
		cliService.delete(id);
		return new ResponseEntity<Client>(HttpStatus.NO_CONTENT);
	}

}
