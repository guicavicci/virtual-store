package com.brprev.payload;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class ProductDTO {
	
	@NotNull(message = "Name cannot be null")
	private String name;
	
	@NotNull(message = "Price cannot be null")
	private Float price;
	
	private String category;	

}
