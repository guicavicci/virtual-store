package com.brprev.payload;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import lombok.Data;

@Data
public class ClientDTO {
	
	@NotNull(message = "Name cannot be null")
	private String name;
	
	@CPF(message = "Social sec number invalid")
	private String socialSec;
	
	@Email(message = "Email should be valid")
	private String email;	

}
