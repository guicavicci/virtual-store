package com.brprev.payload;

import java.util.ArrayList;
import java.util.List;

import com.brprev.models.Client;
import com.brprev.models.Product;

import lombok.Data;

@Data
public class OrderDTO {
	
	private Client client;
	
	private List<Product> products = new ArrayList<>();
	//private Set<Product> products;

}
