package com.brprev.messages.config;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.brprev.services.EmailService;
import com.google.gson.Gson;


@Service
public class ClientKafkaSub {
	
	@Autowired
	private EmailService emailService;
	
	@KafkaListener(topics = "createClient", groupId = "createClientGroup")
	public void listen(String message) {
	    System.out.println("Received Messasge in group foo: " + message);
	    
	    Map<String, String> map = new Gson().fromJson(message, Map.class);
	    
	    String messageBody = "Welcome mr or mrs " + map.get("name") + "!!!";
	    
	    emailService.sendMail(map.get("email"), "Welcome Virtual Store", messageBody);
	    
	}

}
