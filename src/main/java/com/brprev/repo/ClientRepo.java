package com.brprev.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.brprev.models.Client;

public interface ClientRepo extends JpaRepository<Client, Long>{

}
