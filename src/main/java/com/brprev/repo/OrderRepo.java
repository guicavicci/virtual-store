package com.brprev.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.brprev.models.Order;

public interface OrderRepo extends JpaRepository<Order, Long>{

}
