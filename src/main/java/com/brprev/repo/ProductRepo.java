package com.brprev.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.brprev.models.Product;

public interface ProductRepo extends JpaRepository<Product, Long>{

}
