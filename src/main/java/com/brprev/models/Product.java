package com.brprev.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "product")
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "price")
	private Float price;
	
	@Column(name = "category")
	private String category;
	
	/*
	 @ManyToMany(mappedBy = "orders")
	 Set<Order> orders;
	 */
	 
	 /*@OneToMany(mappedBy = "product")
	 Set<OrderProduct> ratings;
	 */
	@JsonIgnore
	public boolean isValid() {
	    return name != null && price != null && category != null;
	  }
}
