package com.brprev.models;

import javax.persistence.Table;

import lombok.Data;

@Data
@Table(name = "order_product")
public class OrderProduct {
	private Long product;
	
	OrderProduct(long product) {
		this.product = product;
	}
}