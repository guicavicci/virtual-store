package com.brprev.services;

import com.brprev.models.Order;
import com.brprev.payload.OrderDTO;

import javassist.NotFoundException;

public interface OrderServiceInterface {
	
	Order save(OrderDTO orderDTO) throws NotFoundException;
	
	Order findById(long id) throws NotFoundException;
	
	void delete(long id);

}
