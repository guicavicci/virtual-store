package com.brprev.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brprev.models.Product;
import com.brprev.payload.ProductDTO;
import com.brprev.repo.ProductRepo;

import javassist.NotFoundException;

@Service
public class ProductService implements ProductServiceInterface{

	@Autowired
	private ProductRepo prodRepo; 

	Product prod = new Product();

	public Product save(ProductDTO prodDTO){
		prod.setName(prodDTO.getName());
		prod.setPrice(prodDTO.getPrice());
		prod.setCategory(prodDTO.getCategory());

		prod = prodRepo.save(prod);

		if (!prod.isValid()) {
			throw new NullPointerException("product invalid");
		}

		return prod;

	}
	
	public Product findById(long id) throws NotFoundException {
		Optional<Product> opProd = prodRepo.findById(id);
		
		if (opProd.isPresent()) {
			prod = opProd.get();
			return prod;
		} else {
			throw new NotFoundException("produto not found");
		}
	}
	
	public void delete(long id) {
		prodRepo.deleteById(id);
	}

}
