package com.brprev.services;

import com.brprev.models.Product;
import com.brprev.payload.ProductDTO;

import javassist.NotFoundException;

public interface ProductServiceInterface {
	
	Product save(ProductDTO prodDTO);
	
	Product findById(long id) throws NotFoundException;
	
	void delete(long id);
	

}
