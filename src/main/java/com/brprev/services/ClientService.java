package com.brprev.services;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.brprev.models.Client;
import com.brprev.payload.ClientDTO;
import com.brprev.repo.ClientRepo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javassist.NotFoundException;

@Service
public class ClientService implements ClientServiceInterface{

	@Autowired
	private ClientRepo cliRepo; 
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	Client client = new Client();

	public Client save(ClientDTO cliDTO, boolean isCreate){
		client.setName(cliDTO.getName());
		client.setSocialSec(cliDTO.getSocialSec());
		client.setEmail(cliDTO.getEmail());

		client = cliRepo.save(client);

		if (!client.isValid()) {
			throw new NullPointerException("client invalid");
		}
		
		if (isCreate) {
			System.out.println("DEBUG");
			System.out.println(client.getName());
			String cliJson = clientToJson(client);
			kafkaTemplate.send("createClient", cliJson);
		}

		return client;

	}
	
	public Client findById(long id) throws NotFoundException {
		Optional<Client> opClient = cliRepo.findById(id);
		
		if (opClient.isPresent()) {
			client = opClient.get();
			return client;
		} else {
			throw new NotFoundException("client not found");
		}
	}
	
	public void delete(long id) {
		cliRepo.deleteById(id);		
	}
	
	public String clientToJson(Client cli) {
		Map mapClient = new HashMap<String, String>();
		mapClient.put("name", cli.getName());
		mapClient.put("email", cli.getEmail());
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(mapClient);
		
		return json;
		
	}

}
