package com.brprev.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.brprev.models.Client;
import com.brprev.models.Order;
import com.brprev.models.OrderProduct;
import com.brprev.models.Product;
import com.brprev.payload.OrderDTO;
import com.brprev.repo.OrderRepo;

import javassist.NotFoundException;

@Service
public class OrderService implements OrderServiceInterface{

	@Autowired
	private OrderRepo orderRepo;
	
	Order order = new Order();
	
	Client cli = new Client();
	
	List<Product> products = new ArrayList<Product>();
	
	List<OrderProduct> orderProducts = new ArrayList<OrderProduct>();

	public Order save(OrderDTO orderDTO) throws NotFoundException{
		order.setClient(orderDTO.getClient());
		
		//DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		order.setDate(date);
		
		products = orderDTO.getProducts();
		
		for (Product product : products) {
			order.getProducts().add(product);
		}
		
		order = orderRepo.save(order);

		if (order == null) {
			throw new NullPointerException("order invalid");
		}

		return order;
	}
	
	
	public Order findById(long id) throws NotFoundException {
		Optional<Order> opOrder = orderRepo.findById(id);
		
		if (opOrder.isPresent()) {
			order = opOrder.get();
			return order;
		} else {
			throw new NotFoundException("order not found");
		}
	}
	
	
	public void delete(long id) {
		orderRepo.deleteById(id);
	}
	

}
