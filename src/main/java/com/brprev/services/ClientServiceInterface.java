package com.brprev.services;

import org.springframework.web.bind.annotation.RequestBody;

import com.brprev.models.Client;
import com.brprev.payload.ClientDTO;

import javassist.NotFoundException;

public interface ClientServiceInterface {
	
	Client save(@RequestBody ClientDTO cliDTO, boolean isCreate);
	Client findById(long id) throws NotFoundException;
	void delete(long id);

}
